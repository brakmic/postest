import json
import threading

class PsDict(dict) :
    """
    Thread-safe dictionary
    """
    def __init__(self, *args, **kwargs) :
        dict.__init__(self, *args, **kwargs)
        self._lock = threading.Lock()

    def __enter__(self) :
        self._lock.acquire()
        return self

    def __exit__(self, type, value, traceback):
        self._lock.release()

class PsConfig(object):
    def __init__(self, accounts, databases, servers, runtime, *args, **kwargs):
        self.accounts = accounts
        self.databases = databases or {}
        self.servers = servers or { 
                                    "master": "missing", 
                                    "caching": "missing" 
                                  }
        self.runtime = runtime or { "test_against": "master" }


def get_config(config_file=None):
    try:
        if config_file is None:
            config_file = 'config.json'
        with open(config_file, 'r') as f:
            data = json.loads(f.read())
            return PsConfig(**data)
    except Exception as e:
        print(e)
        return None
