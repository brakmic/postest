from locust import HttpUser, TaskSet, task, between, events
import uuid
import json
from ps import PsDict, PsConfig, get_config
from ps_searches import PsSearchBundle

class PsEnvironment(object):
    """
    Helper class for runtime data
    """
    searches = PsDict()
    search_bundle = PsSearchBundle()

    def add_search(self, unique_name, task_group_name, response_json):
        """
        This function is being used to persist searches in local memory
        """
        self.searches[unique_name] = {
            "task_group_name": task_group_name,
            "json": response_json
        }
    def get_search(self, unique_name):
        """
        For querying saved searches from local memory
        """
        return self.searches.get(unique_name)


class PsUserBehavior(TaskSet):
    """
    Base class for all tests
    """
    config = None
    token = None
    server = None
    
    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.readconfig()
        self.login()

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """
        pass

    def readconfig(self):
        """
        Read configuration from config.json (default)
        """
        self.config = get_config()
        self.server = self.config.servers[self.config.runtime['test_against']]

    def login(self):
        """
        Login function that connects to PatentSight servers and persists secure tokens
        """
        if self.token == None:
            self.client.headers["Content-Type"] = "application/json"
            payload = {
                        "login": self.config.accounts['default_username'],
                        "password": self.config.accounts['default_password']
                    }
            response = self.client.post("{}/user/login2".format(self.server), json=payload, name="Login")
            self.token = response.json()
            self.client.headers["X-PatentSight-SecureToken"] = self.token["token"]

class SavedSearch(PsUserBehavior):
    """
    Base class for tests\n
    Contains code for account management and cleanups
    """
    folderId = None
    env = PsEnvironment()
    unique_names = PsDict()

    def on_start(self):
        """
        Default on_start function from Locust framework.\n
        Also reads the testing user's folderId that will be used during testing.
        """
        super().on_start()
        self.folderId = self.config.runtime['folderId']

    def on_stop(self):
        """
        This function is being called by Locust framework.\n
        It also removes all saved searches from testing user's folder.
        """
        super().on_stop()
        if self.env != None:
            for k, v in self.env.searches.copy().items():
                payload = {
                        "objects": [
                            {
                                "persistedID": v['json']['savedSearch']['persistedID'],
                                "objectType": v['json']['savedSearch']['objectType']
                            }
                        ]
                    }
                response = self.client.delete("/bulk/delete", json=payload, name='Cleanup')
                if response.status_code != 200:
                    print(response.text)

    def process_response(self, response, search_name, search_name_dict, task_group_name):
        if response.status_code == 200:
            self.unique_names[search_name_dict] = search_name
            self.env.add_search(search_name, task_group_name, response.json())
        else:
            self.unique_names[search_name_dict] = None
            print(response.text)


class SavedSearchTasks(SavedSearch):
    """
    Contains following tasks:\n
    * One simple saved search\n
    * Complex saved search\n
    * Something simple to be referenced: FilingDate=(2018)\n
    * Simple referencing: ##Search1 AND ##Search2\n
    * All "Fibonacci Referencing" tests\n
    * Load saved search by Id
    """

    def on_start(self):
        super().on_start()

    @task(20)
    def task_save_search_1(self):
        task_group_name = "Search1"
        search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
        payload = self.env.search_bundle.create_simple_search_1(search_name, self.folderId)
        response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
        self.process_response(response, search_name, 'saved_search_1' , task_group_name)

    @task(20)
    def task_save_search_2(self):
        task_group_name = "Search2"
        search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
        payload = self.env.search_bundle.create_simple_search_2(search_name, self.folderId)
        response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
        self.process_response(response, search_name, 'saved_search_2' , task_group_name)

    @task(17)
    def task_simple_referencing(self):
        task_group_name = "Search3"
        search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
        payload = self.env.search_bundle.create_simple_referencing_search(search_name, self.folderId)
        response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
        self.process_response(response, search_name, 'saved_search_3' , task_group_name)

    @task(16)
    def task_reference_two_searches(self):
        search1 = self.unique_names.get('saved_search_1')
        search2 = self.unique_names.get('saved_search_2')
        if search1 == None or search2 == None:
            return
        else:
            task_group_name = "Search4"
            search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
            payload = self.env.search_bundle.create_reference_two_searches(search_name, "##\"{}\"".format(search1), "##\"{}\"".format(search2), self.folderId)
            response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
            self.process_response(response, search_name, 'saved_search_4' , task_group_name)

    @task(15)
    def task_search5_fibonacci(self):
        search4 = self.unique_names.get('saved_search_4')
        if search4 == None:
            return
        else:
            task_group_name = "Search5"
            search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
            payload = self.env.search_bundle.create_fibonacci_reference_1(search_name, "##\"{}\"".format(search4), self.folderId)
            response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
            self.process_response(response, search_name, 'saved_search_5' , task_group_name)

    @task(14)
    def task_search6_fibonacci(self):
        search4 = self.unique_names.get('saved_search_4')
        search5 = self.unique_names.get('saved_search_5')
        if search4 == None or search5 == None:
            return
        else:
            task_group_name = "Search6"
            search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
            payload = self.env.search_bundle.create_fibonacci_reference_2(search_name, "##\"{}\"".format(search5), "##\"{}\"".format(search4), self.folderId)
            response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
            self.process_response(response, search_name, 'saved_search_6' , task_group_name)

    @task(13)
    def task_search7_fibonacci(self):
        search5 = self.unique_names.get('saved_search_5')
        search6 = self.unique_names.get('saved_search_6')
        if search5 == None or search6 == None:
            return
        else:
            task_group_name = "Search7"
            search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
            payload = self.env.search_bundle.create_fibonacci_reference_3(search_name, "##\"{}\"".format(search6), "##\"{}\"".format(search5), self.folderId)
            response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
            self.process_response(response, search_name, 'saved_search_7' , task_group_name)

    @task(12)
    def task_search8_fibonacci(self):
        search6 = self.unique_names.get('saved_search_6')
        search7 = self.unique_names.get('saved_search_7')
        if search6 == None or search7 == None:
            return
        else:
            task_group_name = "Search8"
            search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
            payload = self.env.search_bundle.create_fibonacci_reference_4(search_name, "##\"{}\"".format(search7), "##\"{}\"".format(search6), self.folderId)
            response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
            self.process_response(response, search_name, 'saved_search_8' , task_group_name)

    @task(11)
    def task_search9_fibonacci(self):
        search7 = self.unique_names.get('saved_search_7')
        search8 = self.unique_names.get('saved_search_8')
        if search7 == None or search8 == None:
            return
        else:
            task_group_name = "Search9"
            search_name = "Search_{}".format(str(uuid.uuid4()).replace("-",""))
            payload = self.env.search_bundle.create_fibonacci_reference_5(search_name, "##\"{}\"".format(search8), "##\"{}\"".format(search7), self.folderId)
            response = self.client.post("/savedsearches/createAndShare", json=payload, name=task_group_name)
            self.process_response(response, search_name, 'saved_search_9' , task_group_name)

    @task(10)
    def task_get_saved_search_by_id(self):
        if len(self.unique_names) == 0:
            return
        else:
            task_group_name = "Load by Id"
            for k, v in self.unique_names.copy().items():
                saved_search = self.env.get_search(v)
                if saved_search == None:
                    return
                else:
                    response = self.client.get("/savedsearches/{}".format(saved_search['json']['savedSearch']['persistedID']), name="{} - {}".format(task_group_name, k))

    
class MasterWebsiteUser(HttpUser):
    config = get_config()
    tasks = [SavedSearchTasks]
    wait_time = between(1, 1)
    host = None
    if config.runtime['test_against'] == 'master':
        host = config.servers['master']
    else:
        host = config.servers['caching']
    print('Running tests against {}\n'.format(host))

if __name__ == '__main__':
    MasterWebsiteUser().run()

